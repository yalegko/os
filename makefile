BUILDDIR = build
SRCDIR = src
DEBUGDIR = debug
arch ?= i686-unknown-linux-gnu
kernel_rust_file := target/$(arch)/debug/libkernel.a

disk.img-kernel: disk.img-text kernel
	@dd \
		if=$(BUILDDIR)/kernel \
		of=$(BUILDDIR)/disk.img \
		bs=512 \
		count=1000 \
		seek=2 \
		conv=notrunc

kernel: kernel-asm kernel-rust kernel-dummy
	@ld \
		-n \
		-m elf_i386 \
		-T linker.ld \
		--oformat binary \
		-o $(BUILDDIR)/kernel \
		$(BUILDDIR)/kernel-asm \
		$(BUILDDIR)/kernel-rust \
		$(BUILDDIR)/dummy


kernel-rust: $(SRCDIR)/kernel.rs Cargo.toml
	cargo rustc \
		--target=$(arch) \
		-- \
		-C relocation-model=static \
		-Z no-landing-pads
	@cp $(kernel_rust_file) $(BUILDDIR)/kernel-rust

kernel-asm: $(SRCDIR)/kernel.asm $(SRCDIR)/gdt.asm
	@nasm \
		-f elf \
		-o $(BUILDDIR)/kernel-asm \
		$(SRCDIR)/kernel.asm

kernel-dummy: $(SRCDIR)/dummy.asm
	@nasm \
		-f elf \
		-o $(BUILDDIR)/dummy \
		$(SRCDIR)/dummy.asm

disk.img-text: disk.img-boot
	@dd \
		if=$(SRCDIR)/text \
		of=$(BUILDDIR)/disk.img \
		bs=512 \
		count=1 \
		seek=1 \
		conv=notrunc

disk.img-boot: disk.img-raw boot
	@dd \
		if=$(BUILDDIR)/boot \
		of=$(BUILDDIR)/disk.img \
		bs=512 \
		count=1 \
		conv=notrunc

boot: $(SRCDIR)/boot.asm
	@nasm \
		-f bin \
		-o $(BUILDDIR)/boot \
		$(SRCDIR)/boot.asm

disk.img-raw:
	@dd \
		if=/dev/zero \
		of=$(BUILDDIR)/disk.img \
		bs=1M \
		count=1

build: disk.img-kernel
	cp $(BUILDDIR)/disk.img $(DEBUGDIR)/disk.img

clean:
	rm -rf $(BUILDDIR)/*
	rm -f $(DEBUGDIR)/disk.img
	cargo clean
