; https://en.wikibooks.org/wiki/X86_Assembly/Global_Descriptor_Table
; http://wiki.osdev.org/Global_Descriptor_Table
; http://files.osdev.org/mirrors/geezer/os/pm.htm
; http://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf

; struc gdt_entry_struct
;        limit_low:             resb 2
;        base_low:              resb 2
;        base_middle:           resb 1
;        access:                resb 1
;        flags, limit_high :    resb 1 ( 4 bit + 4 bit )
;        base_high:             resb 1
; endstruct

; Acess bits from 7 to 0
; - Present bit. Must be set to one to permit segment access.
; - Privilege bit 1. Zero is the highest level of privilege (Ring 0), three is the lowest (Ring 3).
; - Privilege bit 2
; - Executable bit. If one, this is a code segment, otherwise it's a stack/data segment.
; - Expansion direction (stack/data segment). If one, segment grows downward, and offsets within the segment must be greater than the limit.
; - Conforming (code segment). Privilege-related.
; - Writable (stack/data segment) / Readable (code segment)
; - Accessed. This bit is set whenever the segment is read from or written to.

; Flags
; - Granularity: 1 (if set multiplies limit by 4 K, allowing our segment to span 4 Gb of memory)
; - 32-bit default: 1
; - 64-bit code segment: 0, unused on 32-bit processor
; - AVL: 0

gdt_start:
    gdt_head: 
        dd 0x77
        dd 0x77

    gdt_cs:
        dw 0xFFFF       ; limit_low
        dw 0x0000       ; base_low
        db 0x0000       ; base_middle
        db 10011010b    ; access 
        db 11001111b    ; flags + limit
        db 0x0000       ; base_high

    gdt_ds:
        dw 0xFFFF       ; limit_low
        dw 0x0000       ; base_low
        db 0x0000       ; base_middle
        db 10010010b    ; access
        db 11001111b    ; flags + limit
        db 0x0000       ; base_high
gdt_end: 

gdt:
    dw gdt_end - gdt_start - 1 ; GDT size
    dd gdt_start

