#![feature(no_std,lang_items,asm,core_str_ext,core_slice_ext,const_fn)]
#![no_std]

pub use core::fmt::Write;

#[macro_use]
mod screen;
#[macro_use]
pub mod multitasking;
pub mod idt;

fn sleep(n: u32) {
    let mut sleep = 1;
    while sleep != n {
        sleep += 1;
    }
}

fn process1() {
    loop {
        sleep(3000000);
        println!("Hi I'm process1");
    }
}

fn process2() {
    loop {
        sleep(3000000/2);
        println!("Hi I'm process2");
    }
}

fn process3() {
    loop {
        sleep(3000000/3);
        println!("Hi I'm process3");
    }
}

#[no_mangle]
pub extern fn rust_main(){
    unsafe { idt::idt_install(); }

    screen::println_number(1337);
    /*
    let welcome = "Hello Mellow";
    for i in 0..15 {
        println!("{}. {}!", i, welcome);
    }
    */

    initilize_task_manager!();
    create_process!(process1);
    create_process!(process2);
    create_process!(process3);
    task_manager_start!();
}
