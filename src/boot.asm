; 0x7c00 - first command
; 0xb8000 - video memory
; word 0x7e00 == current cursor
; 0x8000 - memory to load

VIDEO_MEM equ 0xb800
CURSOR_POS equ 0x7e00 ; (word)
KERNEL_OFFSET equ 0x800
STACK equ 0x7c00
;disk geometry
SectorsPerTrack equ 8
NumberOfHeads equ 16



ORG 0x7c00

;allocate stack
xor ax,ax
mov ds, ax
mov ss, ax
mov sp, STACK

jmp 0x0:boot
boot:
    call load_kernel

    ;some printing stuff
    call cls

    mov ax, welcome_str
    push ax

    xor ax,ax
    mov al, welcome_len
    push ax

    call print_string

    jmp KERNEL_OFFSET:0x0

load_kernel:
    ;need to load 540 sectors starting from 3rd
    mov si, 600
    mov ax, 2
    ;will load to ES:BX starting from KERNEL_OFFSET
    mov bx, KERNEL_OFFSET
    mov es, bx

    .loop:
        push ax
        call LBA_to_CHS
        shl cx, 8     ;mov cl to ch so cylinder will be in ch
        mov cl, al    ;sector
        mov dh, bl    ;head
        call read_from_disk

        pop ax
        cmp ax, si
            je .end

        inc ax
        mov bx, es
        add bx, 32  ;that is 512/16 cos we add it to ES
        mov es, bx
    jmp .loop

    .end:
        ret

; Input:  ax - LBA value
; Output: ax - Sector
;	        bx - Head
;	        cx - Cylinder
LBA_to_CHS:
    xor dx, dx
    mov bx, SectorsPerTrack
    div bx

    ; result in ax*bx + dx
    inc dx    ;make sector number to be nonzero
    push dx

    xor dx, dx
    mov bx, NumberOfHeads
    div bx

    ;same as before:
    mov bx, dx  ;dx -> Head number
    mov cx, ax  ;ax -> Cylinder number
    pop ax      ;stored Sector number
ret


read_from_disk:
    mov ah, 0x02
    ;AL = number of sectors to read (must be nonzero)
    mov al, 0x01
    ;CH = low eight bits of cylinder number
    ;CL = sector number 1-63 (bits 0-5)
    ;     high two bits of cylinder (bits 6-7, hard disk only)

    ;DH = head number
    ;DL = drive number (bit 7 set for hard disk)
    mov dl, 0x80

    ;ES:BX = data buffer (es*16+bx)
    xor bx, bx
    int 0x13
ret

%include "src/procedures.asm"

welcome_str db "Hello, yoba, eto ti?"
welcome_len equ ($-welcome_str)

times 510-($-$$) db 0x00
db 0x55, 0xaa
