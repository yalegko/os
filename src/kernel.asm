extern rust_main
BITS 16
;segment start at 0x80000
;org 0x8000

    ;disable interrupts
    cli

    ;set protected mode
    mov eax, cr0
    or eax, 0x1
    mov cr0, eax

    ;now enable SSE and the like
    mov eax, cr0
    and ax, 0xFFFB	   ;clear coprocessor emulation CR0.EM
    or ax, 0x2			   ;set coprocessor monitoring  CR0.MP
    mov cr0, eax
    mov eax, cr4
    or ax, 3 << 9		   ;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
    mov cr4, eax

    ;load Global Descriptor Table
    lgdt [gdt]
    jmp 0x8:protected_mode

%include "src/gdt.asm"

BITS 32
;segment starts at 0x0
protected_mode:

	;clear segment registers
	mov eax, 0x10
	mov es, eax
	mov ds, eax
	mov ss, eax
	mov gs, eax

	;setup stack
	mov esp, stack_top

	;let's start
	call rust_main
  .loop1:
    jmp .loop1

section .bss
  stack_bottom:
    resb 104096
  stack_top:
