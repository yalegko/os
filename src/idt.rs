#![allow(unused_unsafe)]

pub use core::iter::Iterator;
extern crate cpuio;
use self::cpuio::{Port, UnsafePort};

extern {
    fn asm_int_handler_32();
    fn asm_int_handler_33();
}

macro_rules! create_function {
    ($func_name:ident) => (
        unsafe fn $func_name() {
            asm!("cli");
            //println!("You called me: {}",  stringify!($func_name));
            asm!("leave; iret");
        }
    )
}

/* Defines an IDT entry */
#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct IDTEntry {
    base_lo: u16,    // offset bits 0..15
    sel: u16,        // a code segment selector in GDT or LDT
    zero: u8,        // unused, set to 0
    flags: u8,       // type and attributes, see at http://wiki.osdev.org/Interrupt_Descriptor_Table
    base_hi: u16     // offset bits 16..31
}

/* Defines an IDT pointer */
#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct IDTPointer {
    size: u16,
    base: u32
}

/* Declare an IDT of 256 entries. If any undefined IDT entry is hit,
 * it normally will cause an "Unhandled Interrupt" exception. Any
 * descriptor for which the 'presence' bit is cleared (0) will generate
 * an "Unhandled Interrupt" exception */
pub static mut idt: [IDTEntry; 256] = [IDTEntry {base_lo: 0, sel: 0x08, zero: 0, flags: 0, base_hi: 0}; 256];
pub static mut idtp: IDTPointer = IDTPointer {size: 0, base: 0};

/* Use this function to set an entry in the IDT. */
fn idt_set_gate(num: usize, f: unsafe fn(), sel: u16, flags: u8) {
    unsafe {
        let base = f as u32;
        idt[num].sel = sel;
        idt[num].flags = flags;
        idt[num].base_hi = (base >> 16) as u16;
        idt[num].base_lo = (base & ((1 << 16) - 1)) as u16;
    }
}

fn idt_set_gate_asm(num: usize, f: unsafe extern "C" fn(), sel: u16, flags: u8) {
    unsafe {
        let base = f as u32;
        idt[num].sel = sel;
        idt[num].flags = flags;
        idt[num].base_hi = (base >> 16) as u16;
        idt[num].base_lo = (base & ((1 << 16) - 1)) as u16;
    }
}

/* The following code will be used to remap PICs. More info at
 * http://www.randomhacks.net/2015/11/16/bare-metal-rust-configure-your-pic-interrupts/
 * http://wiki.osdev.org/PIC
 */

/* Some named ports and commands for future use*/
// Command sent to begin PIC initialization.
const CMD_INIT: u8 = 0x11;
// Command sent to acknowledge an interrupt.
const CMD_END_OF_INTERRUPT: u8 = 0x20;
// The mode in which we want to run our PICs.
const MODE_8086: u8 = 0x01;

struct Pic {
    offset: u8,
    command: UnsafePort<u8>,
    data: UnsafePort<u8>,
}

impl Pic {
    // Check if that PIC should handle interrupt
    fn handles_interrupt(&self, interupt_id: u8) -> bool {
        self.offset <= interupt_id && interupt_id < self.offset + 8
    }

    unsafe fn end_of_interrupt(&mut self) {
        self.command.write(CMD_END_OF_INTERRUPT);
    }
}

pub struct ChainedPics {
    pics: [Pic; 2],
}

impl ChainedPics {
    pub const unsafe fn new(offset1: u8, offset2: u8) -> ChainedPics {
        ChainedPics {
            pics: [
                Pic {
                    offset: offset1,
                    command: cpuio::UnsafePort::new(0x20),
                    data: cpuio::UnsafePort::new(0x21),
                },
                Pic {
                    offset: offset2,
                    command: cpuio::UnsafePort::new(0xA0),
                    data: cpuio::UnsafePort::new(0xA1),
                },
            ]
        }
    }

   pub unsafe fn initialize(&mut self) {
       // Will write some garbage to 0x80 port to make a lil pause
       // (They said it's ok even for older versions of Linux)
       let mut wait_port: cpuio::Port<u8> = cpuio::Port::new(0x80);
       let mut wait = || { wait_port.write(0) };

       let saved_mask1 = self.pics[0].data.read();
       let saved_mask2 = self.pics[1].data.read();

       // Tell each PIC that we're going to send it initialization sequences.
       self.pics[0].command.write(CMD_INIT);
       wait();
       self.pics[1].command.write(CMD_INIT);
       wait();

       // Byte 1: Set up base offsets.
       self.pics[0].data.write(self.pics[0].offset);
       wait();
       self.pics[1].data.write(self.pics[1].offset);
       wait();

       // Byte 2: Configure chaining between PIC1 and PIC2.
       self.pics[0].data.write(4);
       wait();
       self.pics[1].data.write(2);
       wait();

       // Byte 3: Set the mode.
       self.pics[0].data.write(MODE_8086);
       wait();
       self.pics[1].data.write(MODE_8086);
       wait();

       // Restore saved masks.
       self.pics[0].data.write(saved_mask1);
       self.pics[1].data.write(saved_mask2);
   }

   pub fn handles_interrupt(&self, interrupt_id: u8) -> bool {
       self.pics.iter().any(|p| p.handles_interrupt(interrupt_id))
   }

   // Check which of PICs should handle IRQ
   pub unsafe fn notify_end_of_interrupt(&mut self, interrupt_id: u8) {
       if self.handles_interrupt(interrupt_id) {
           if self.pics[1].handles_interrupt(interrupt_id) {
               self.pics[1].end_of_interrupt();
           }
           self.pics[0].end_of_interrupt();
       }
   }
}

create_function!(int_handler_0);
create_function!(int_handler_1);
//unsafe fn int_handler_1(){loop {}}
create_function!(int_handler_2);
create_function!(int_handler_3);
create_function!(int_handler_4);
create_function!(int_handler_5);
create_function!(int_handler_6);
//unsafe fn int_handler_6(){loop {}}
create_function!(int_handler_7);
create_function!(int_handler_8);
create_function!(int_handler_9);
create_function!(int_handler_10);
create_function!(int_handler_11);
create_function!(int_handler_12);
create_function!(int_handler_13);
//unsafe fn int_handler_13(){loop {} unsafe{asm!("int $$0xff")}}
create_function!(int_handler_14);
create_function!(int_handler_15);
create_function!(int_handler_16);
create_function!(int_handler_17);
create_function!(int_handler_18);
create_function!(int_handler_19);
create_function!(int_handler_20);
create_function!(int_handler_21);
create_function!(int_handler_22);
create_function!(int_handler_23);
create_function!(int_handler_24);
create_function!(int_handler_25);
create_function!(int_handler_26);
create_function!(int_handler_27);
create_function!(int_handler_28);
create_function!(int_handler_29);
create_function!(int_handler_30);
create_function!(int_handler_31);
create_function!(int_handler_32);
create_function!(int_handler_33);
create_function!(int_handler_34);
create_function!(int_handler_35);
create_function!(int_handler_36);
create_function!(int_handler_37);
create_function!(int_handler_38);
create_function!(int_handler_39);
create_function!(int_handler_40);
create_function!(int_handler_41);
create_function!(int_handler_42);
create_function!(int_handler_43);
create_function!(int_handler_44);
create_function!(int_handler_45);
create_function!(int_handler_46);
create_function!(int_handler_47);
create_function!(int_handler_48);

unsafe fn display_write_int() {
    use screen;
    use core::{slice, str};
    use screen::{Printing, Color};

    asm!("cli");

    let ptr = screen::display.buffer.str_pointer;
    let strlen = screen::display.buffer.str_length;
    let color = screen::display.buffer.color;

    // Transform buffer it into something reasonable
    let bytes = slice::from_raw_parts(ptr, strlen);
    let s = str::from_utf8_unchecked(bytes);

    screen::display.print_str(s, color);

    asm!("leave; iret");
}

static mut interrupts: [unsafe fn(); 50] = [int_handler_0,int_handler_1,int_handler_2,int_handler_3,int_handler_4,int_handler_5,int_handler_6,int_handler_7,int_handler_8,int_handler_9,int_handler_10,int_handler_11,int_handler_12,int_handler_13,int_handler_14,int_handler_15,int_handler_16,int_handler_17,int_handler_18,int_handler_19,int_handler_20,int_handler_21,int_handler_22,int_handler_23,int_handler_24,int_handler_25,int_handler_26,int_handler_27,int_handler_28,int_handler_29,int_handler_30,int_handler_31,
                                            int_handler_32,
                                            int_handler_33,int_handler_34,int_handler_35,int_handler_36,int_handler_37,int_handler_38,int_handler_39,int_handler_40,int_handler_41,int_handler_42,int_handler_43,int_handler_44,int_handler_45,int_handler_46,int_handler_47,int_handler_48,
                                            display_write_int];

static mut PICS : ChainedPics = unsafe {ChainedPics::new(0x20, 0x28)};

#[no_mangle]
pub unsafe extern fn int_timer() {
    PICS.notify_end_of_interrupt(32);
}
#[no_mangle]
pub unsafe extern fn int_keyboard() {
    use multitasking;
    use multitasking::ProcessManagement;

    let mut keyboard: Port<u8> = unsafe { Port::new(0x60) };
    // So the key '1' will control process 1
    let scancode = keyboard.read() - 2;
    PICS.notify_end_of_interrupt(33);

    match multitasking::task_manager {
        Some(ref mut tm) => tm.toogle_process(scancode),
        None => ()
    }
}

pub unsafe fn idt_install() {
    /* Sets the special IDT pointer up  */
    idtp.size = ((super::core::mem::size_of::<IDTEntry>() * 256) - 1) as u16;
    idtp.base = &idt as *const _ as u32;

    /* Add any new ISRs to the IDT here using idt_set_gate */
    for (i,int) in interrupts.iter().enumerate() {
        if i == 33 {
            idt_set_gate_asm(32, asm_int_handler_32, 0x08, 0x8e);
            idt_set_gate_asm(33, asm_int_handler_33, 0x08, 0x8e);
        } else {
            idt_set_gate(i, *int, 0x08, 0x8E);
        }
    }

    PICS.initialize();
    /* Turn interrupts on */
    asm!("lidt ($0)" :: "r" (idtp));
    asm!("sti");
}
