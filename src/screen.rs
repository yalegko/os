#![allow(unused_unsafe)]

extern crate rlibc;
use core::str;

const DISPLAY_ROWS: u32 = 25;
const DISPLAY_COLS: u32 = 80;
const VIDEO_MEM: u32 = 0xb8000;

#[allow(dead_code)]
#[repr(u8)]
#[derive(Copy, Clone)]
pub enum Color {
    Black      = 0,
    Blue       = 1,
    Green      = 2,
    Cyan       = 3,
    Red        = 4,
    Magenta    = 5,
    Brown      = 6,
    LightGray  = 7,
    DarkGray   = 8,
    LightBlue  = 9,
    LightGreen = 10,
    LightCyan  = 11,
    LightRed   = 12,
    Pink       = 13,
    Yellow     = 14,
    White      = 15,
}

impl Color {
    pub fn from_u8(n: u8) -> Color {
        match n {
            0 => Color::Black,
            1 => Color::Blue,
            2 => Color::Green,
            3 => Color::Cyan,
            4 => Color::Red,
            5 => Color::Magenta,
            6 => Color::Brown,
            7 => Color::LightGray,
            8 => Color::DarkGray,
            9 => Color::LightBlue,
            10 => Color::LightGreen,
            11 => Color::LightCyan,
            12 => Color::LightRed,
            13 => Color::Pink,
            14 => Color::Yellow,
            15 => Color::White,
            _ => Color::White
        }
    }
}

struct Cursor { x: u32, y: u32 }
trait CursorPositioning  {
    fn get_cursor_offset(&self) -> u32;
    fn move_cursor(&mut self);
    fn new_line(&mut self);
}

pub struct ScreenBuffer {
    pub str_pointer: * const u8,
    pub str_length: usize,
    pub color: Color
}

pub struct Screen {
    width: u32,
    height: u32,
    cursor: Cursor,
    pub buffer: ScreenBuffer
}

impl Default for Screen {
    fn default() -> Screen {
        Screen {
             width: DISPLAY_COLS,
             height: DISPLAY_ROWS,
             cursor: Cursor { x:0, y:2 },
             buffer: ScreenBuffer {
                 str_pointer: 0 as *const u8,  //sadly nullptr, thanks to lifetimes
                 str_length: 0,
                 color: Color::White
             }
       }
    }
}
impl CursorPositioning for Screen {
    fn get_cursor_offset(&self) -> u32 {
        return (self.cursor.y * self.width + self.cursor.x)*2
    }
    fn move_cursor(&mut self) {
        if self.cursor.x == self.width  {
            self.new_line();
        } else {
            self.cursor.x += 1;
        }
    }
    fn new_line(&mut self) {
        if self.cursor.y == self.height {
            unsafe {
                rlibc::memmove(
                    VIDEO_MEM as *mut u8,                       // dest
                    (VIDEO_MEM + self.width * 2) as *mut u8,    // src
                    (self.width * self.height * 2) as usize     // n bytes
                );
                rlibc::memset(
                    (VIDEO_MEM + self.width * (self.height) * 2) as *mut u8,
                    0 as i32,
                    (self.width * 2) as usize                  // n bytes
                );
            }

        } else {
            self.cursor.y += 1;
        }
        self.cursor.x = 0;
    }
}

trait VideoMemoryFunctions {
    fn memory_set(&self, offset: u32, value: u8);
}
impl VideoMemoryFunctions for Screen {
    fn memory_set(&self, offset: u32, value: u8) {
        let real_addres: u32 = VIDEO_MEM + offset;
        unsafe {
            *(real_addres as *mut u8) = value;
        }
    }
}

pub trait Printing {
    fn print_char(&mut self, c: u8, color: Color);
    fn print_str(&mut self, s: &str, color: Color);
    fn clear(&mut self);
}
impl Printing for Screen {
    fn print_char(&mut self, c: u8, color: Color) {
        match c {
            b'\n' => self.new_line(),
            c => {
                let offset = self.get_cursor_offset();
                self.memory_set(offset, c);
                self.memory_set(offset+1, color as u8);
                self.move_cursor();
            }
        }
    }

    fn print_str(&mut self, s: &str, color: Color) {
        for c in s.bytes().into_iter() {
            self.print_char(c, color);
        }
    }
    fn clear(&mut self) {
        unsafe {
            rlibc::memset(
                VIDEO_MEM as *mut u8,                       // dest
                0 as i32,                                   // value
                (self.width * self.height * 2) as usize     // n bytes
            );
        }
    }
}

impl ::core::fmt::Write for Screen {
    fn write_str(&mut self, s: &str) -> ::core::fmt::Result {
        self.buffer.str_pointer = s.as_ptr();
        self.buffer.str_length = s.len();
        self.buffer.color = Color::LightGray;
        unsafe {asm!("int $$49;");}
        Ok(())
    }
}

pub static mut display: Screen = Screen {
     width: DISPLAY_COLS,
     height: DISPLAY_ROWS,
     cursor: Cursor { x:0, y:2 },
     buffer: ScreenBuffer {
         str_pointer: 0 as *const u8,  //sadly nullptr, thanks to lifetimes
         str_length: 0,
         color: Color::White
     }
};

#[macro_export]
macro_rules! println {
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ({
            use core::fmt::Write;
            unsafe { $crate::screen::display.write_fmt(format_args!($($arg)*)).unwrap(); }
    });
}

pub fn print_number(n: u32) {
    print_based_num(n, 10);
}

pub fn print_based_num(n: u32, base: u32) {
    let mut tmp_bytes: [u8;17] = [0;17];
    let mut last_i = 17;
    let mut tmp = n;
    while tmp != 0 {
        last_i -= 1;

        let rem = tmp % base;
        if rem < 10 {
            tmp_bytes[last_i] = (rem) as u8 + 48;
        } else {
            tmp_bytes[last_i] = (rem) as u8 - 10 + 65;
        }

        tmp /= base;
    }

    if base == 16 {
        tmp_bytes[last_i-1] = ('x') as u8;
        tmp_bytes[last_i-2] = ('0') as u8;
        last_i -= 2;
    }

    unsafe {
        use core::fmt::Write;
        let s = str::from_utf8_unchecked(&tmp_bytes[last_i..17]);
        display.write_str(s);
    }
}

pub fn println_number(n: u32) {
    print_number(n);
    print!("\n");
}

#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"] extern fn panic_fmt() {}
