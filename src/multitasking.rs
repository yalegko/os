#![allow(unused_unsafe)]

const MAX_PROC: usize = 4;

/* Define process context for the process queue */
#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct ProcessContext {
    esp: u32,
    is_queued: bool
}

impl Default for ProcessContext {
    fn default() -> ProcessContext {
        ProcessContext {
            esp: 0,
            is_queued: false
        }
    }
}

impl ProcessContext {
    unsafe fn new(entry_point: fn(), stack_top: usize) -> ProcessContext {
        let mut proc_context: ProcessContext = Default::default();

        // Align registers in the stack
        let stack: *mut u32 = stack_top as u32 as *mut u32;
        *stack = 0x200; //eflags with enabled interrupts
        *stack.offset(-1) = 0x8; //cs
        *stack.offset(-2) = entry_point as u32; //eip
        *stack.offset(-3) = 8;  //eax
        *stack.offset(-4) = 7;  //ecx
        *stack.offset(-5) = 6;  //edx
        *stack.offset(-6) = 5;  //ebx
        *stack.offset(-7) = 4;  //esp
        *stack.offset(-8) = 3;  //ebp
        *stack.offset(-9) = 2;  //esi
        *stack.offset(-10) = 1; //edi
        proc_context.esp = stack.offset(-10) as u32;

        // Mark process as ready to execution
        proc_context.is_queued = true;
        proc_context
    }
}

/* Class-like structure to store some globals for multitasking */
#[repr(C,packed)]
pub struct TaskManager {
    current_process: usize,
    is_bored: bool,
    boring_proc: ProcessContext,
    processes: [ProcessContext; MAX_PROC]
}

fn boring_func() {
    loop {
        let mut sleep = 3000000;
        while sleep != 0 {
            sleep -= 1;
        }
        println!("So boring...");
    }
}

impl Default for TaskManager {
    fn default() -> TaskManager {
        let mut process_contexts: [ProcessContext; MAX_PROC] = Default::default();
        for i in 0..MAX_PROC {
            // Give stack for each process
            let stack_frame = (0x80000 + 0x5000 * (i + 1)) as u32;
            process_contexts[i].esp = stack_frame;
        }
        let mut boring: ProcessContext;
        unsafe {
            boring = ProcessContext::new(boring_func, 0x80000);
        }

        TaskManager {
            current_process: 0,
            is_bored: true,
            boring_proc: boring,
            processes: process_contexts
        }
    }
}

pub trait ProcessManagement {
    fn toogle_process(&mut self, n: u8);
    fn get_next_queued(&self) -> Option<usize>;
    fn get_free_process_place(&self) -> Option<usize>;
    fn create_process<'a>(&'a mut self, entry_point: fn())
            -> Result<&'a mut ProcessContext, &'static str>;
    unsafe fn start_next_process(&mut self);
    unsafe fn switch_process(&mut self);
}

impl ProcessManagement for TaskManager {
    fn get_free_process_place(&self) -> Option<usize> {
        for (i, pc) in self.processes.into_iter().enumerate() {
            if !pc.is_queued {
                return Some(i);
            }
        }
        None
    }

    fn create_process<'a>(&'a mut self, entry_point: fn())
            -> Result<&'a mut ProcessContext, &'static str> {
        match self.get_free_process_place() {
            None => Err("Can't find free space for process"),
            Some(i) => {
                unsafe {
                    let prepared_stack =  self.processes[i].esp;
                    self.processes[i] = ProcessContext::new(entry_point, prepared_stack as usize);
                }
                Ok(&mut self.processes[i])
            }
        }
    }

    fn get_next_queued(&self) -> Option<usize> {
        let mut i = self.current_process;
        // Some kind of do while
        while {
            i = (i + 1) % MAX_PROC;
            // Break if we found queued proc or finished the loop
            !self.processes[i].is_queued && i != self.current_process
        }{}
        match self.processes[i].is_queued {
            true => Some(i),
            false => None
        }
    }

    unsafe fn start_next_process(&mut self) {
        let next_proc = match self.get_next_queued() {
            Some(i) => {
                self.current_process = i;
                self.is_bored = false;
                self.processes[i]
            },
            None => {
                self.is_bored = true;
                self.boring_proc
            }
        };
        asm!("
            mov esp, $0;
            popad;
            iretd;
            "::"r"(next_proc.esp):"{esp}":"intel"
        );
    }

    unsafe fn switch_process(&mut self) {
        if self.is_bored {
            asm!(
                "mov $0, [0x500];"
                :"=r"(self.boring_proc.esp):::"intel"
            );
        } else {
            asm!(
                "mov $0, [0x500];"
                :"=r"(self.processes[self.current_process].esp):::"intel"
            );
        };
        self.start_next_process();
    }

    fn toogle_process(&mut self, i: u8) {
        let n: usize = i as usize;
        if n < MAX_PROC {
            self.processes[n].is_queued = !self.processes[n].is_queued;
        }
    }
}

// Global task manager and some macroses
// Used to connect ASM-like and object-oriented models

pub static mut task_manager: Option<TaskManager> = None;

#[macro_export]
macro_rules! initilize_task_manager {
    () => (unsafe {
        $crate::multitasking::task_manager = Some(Default::default());
    })
}

#[macro_export]
macro_rules! create_process {
    ($p:expr) => (unsafe {
        match $crate::multitasking::task_manager {
            None => println!("Task manager is not initilized"),
            Some(ref mut tm) => {
                use multitasking::ProcessManagement;
                match tm.create_process($p) {
                    Err(e) => println!("{}", e),
                    _ => (),
                };
            }
        };
    })
}

#[macro_export]
macro_rules! switch {
    () => (unsafe {
            match $crate::multitasking::task_manager {
            None => println!("Task manager is not initilized"),
            Some(ref mut tm) => {
                use multitasking::ProcessManagement;
                tm.switch_process();
            }
        }
    })
}

#[macro_export]
macro_rules! task_manager_start {
    () => (unsafe {
            match $crate::multitasking::task_manager {
            None => println!("Task manager is not initilized"),
            Some(ref mut tm) => {
                use multitasking::ProcessManagement;
                tm.start_next_process();
            }
        }
    })
}

// Binding for asm calls
#[no_mangle]
pub extern fn switch_fn() {
    switch!();
}
