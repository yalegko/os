global _GLOBAL_OFFSET_TABLE_
global fmod
global fmodf
global asm_int_handler_32
global asm_int_handler_33
extern switch_fn
extern int_timer
extern int_keyboard


_GLOBAL_OFFSET_TABLE_:
    dd 0x77

fmod:
    jmp fmod

fmodf:
    jmp fmodf

asm_int_handler_32:
    cli
    pushad
    mov [0x500], esp
    mov esp, 0x70800
    call int_timer
    call switch_fn
    ;Will iret from the switch_fn to not corrunt the stack

asm_int_handler_33:
    cli
    pushad
    mov [0x500], esp
    mov esp, 0x70800
    call int_keyboard
    mov esp, [0x500]
    popad
    iret
