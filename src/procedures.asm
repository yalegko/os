cls:
    ;es[di] - video memory
    mov ax, VIDEO_MEM
    mov es, ax
    
    ;color 4=red
    mov ah, 0x00
    mov al, 0x20
    mov cx, 2000

    .print_char:
        mov byte [es:di], al
        inc di
        mov byte [es:di], ah
        inc di
    loop .print_char
    ;set cursor to (0;0)
    mov [CURSOR_POS], word 0x0
ret

print_string:
enter 0, 0
    ;es[di] - video memory
    mov ax, VIDEO_MEM
    add ax, word [CURSOR_POS]
    mov es, ax
    
    ;color 4=red
    mov ah,4

    ;si[di] - string, cx - strlen
    xor cx,cx
    mov cx, [bp+4]
    mov si, [bp+6]
    xor di, di
    
    .print_char:
        lodsb
        mov byte [es:di], al
        inc di
        mov byte [es:di], ah
        inc di
    loop .print_char
    
    mov ax, [CURSOR_POS]
    mov cx, [bp+4]
    add ax, cx
    mov [CURSOR_POS], ax
leave
ret

puts:
enter 0, 0
    ;es[di] - video memory
    mov ax, VIDEO_MEM
    add ax, [CURSOR_POS]
    mov es, ax
    
    ;color 4=red
    mov ah, 7

    ;si[di] - string, cx - strlen
    mov si, [bp+4]
    xor di, di

    .print_char:
        lodsb
        test al, al
        jz .end

        mov byte [es:di], al
        inc di
        mov byte [es:di], ah
        inc di
    jmp .print_char

    .end:
    add [CURSOR_POS], di

leave
ret

 
